USE [UDrive]
GO

/****** Object:  StoredProcedure [dbo].[DB_Login]    Script Date: 22/03/2022 10:57:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DB_Login] 
	-- Add the parameters for the stored procedure here
	@Email varchar(100),
	@password varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	select Id_Usuario, Nombre, Picture From Usuario where Email = @Email and Pwd = @password
    -- Insert statements for procedure here
	
END
GO


