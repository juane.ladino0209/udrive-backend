create table Usuario (
	Id_Usuario serial primary key,
	Nombre varchar(50),
	Apellido varchar(50),
	Celular numeric not null,
	Pwd varchar(50) not null,
	Email varchar(100) not null unique,
	Calificacion int default 3,
	Picture varchar,
	create_by varchar(200) default 'sys',
	create_on time default now()
)

select * from Usuario

create table Usuario_Conductor (
	Id_UsuarioConductor serial primary key,
	Id_Usuario serial,
	Fecha_Vencimiento time,
	Calificacion int default 3,
	create_by varchar(200) default 'system_user',
	create_on time default now(),
	foreign key(Id_Usuario)
	references Usuario(Id_Usuario)
)

create Table Automovil (
	Id_Auto serial primary key,
	Id_UsuarioConductor serial,
	Soat bit,
	Placa varchar(8),
	Marca varchar(50),
	Modelo varchar(50),
	Color varchar(50),
	Anho numeric,
	Tarjeta_Propiedad varchar(50),
	create_by varchar(200) default 'system_user',
	create_on time default now(),
	foreign key(Id_UsuarioConductor) references Usuario_Conductor(Id_UsuarioConductor)
)

create table Viaje (
	Id_Viaje serial primary key,
	Id_UsuarioConductor serial,
	Id_Auto serial,
	Fecha_Partida date,
	--Punto_Partida address
	Punto_Partida varchar(50),
	Punto_Llegada varchar(50),
	Is_Active bool default true,
	create_by varchar(200) default 'system_user',
	create_on time default now(),
	foreign key(Id_UsuarioConductor) references Usuario_Conductor(Id_UsuarioConductor),
	foreign key(Id_Auto) references Automovil(Id_Auto)
)

create table Viaje_Usuario (
	Id_ViajeUsuario serial primary key,
	Id_Viaje serial,
	Id_Usuario serial,
	Punto_Salida varchar(50),
	Punto_Llegada varchar(50),
	Fecha_Salida time,
	Fecha_Llegada time,
	Forma_Pago varchar(2),
	Is_Active bool default true,
	create_by varchar(200) default 'system_user',
	create_on time default now(),
	foreign key(Id_Viaje) references Viaje(Id_Viaje),
	foreign key(Id_Usuario) references Usuario(Id_Usuario)
)