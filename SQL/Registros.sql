-- Insert Users

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'alejo@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'sara@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'ladino@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'daniel@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'david@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'esteban@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'oscar@urosario.edu.co')

insert into Usuario(Celular, Pwd, Email)
values('3009876789', 'ABCDEFG12345', 'salome@urosario.edu.co')

-- Inserts Usuario consuctor

insert into Usuario_Conductor(Id_Usuario, Fecha_Vencimiento)
values((select Id_Usuario from Usuario where Email = 'sara@urosario.edu.co'), getdate())


insert into Usuario_Conductor(Id_Usuario, Fecha_Vencimiento)
values((select Id_Usuario from Usuario where Email = 'alejo@urosario.edu.co'), getdate())

insert into Usuario_Conductor(Id_Usuario, Fecha_Vencimiento)
values((select Id_Usuario from Usuario where Email = 'ladino@urosario.edu.co'), getdate())

-- Inserts Automovil

insert into Automovil(Id_UsuarioConductor, Placa, Color, A�o)
values((select Id_UsuarioConductor from Usuario_Conductor where Id_UsuarioConductor = 1), 'AAA111','AZUL','2022')

insert into Automovil(Id_UsuarioConductor, Placa, Color, A�o)
values((select Id_UsuarioConductor from Usuario_Conductor where Id_UsuarioConductor = 2), 'AAA112','AZUL','2022')

insert into Automovil(Id_UsuarioConductor, Placa, Color, A�o)
values((select Id_UsuarioConductor from Usuario_Conductor where Id_UsuarioConductor = 3), 'AAA113','AZUL','2022')

-- Inserts Viajes





