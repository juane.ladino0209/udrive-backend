create table Usuario (
	Id_Usuario int identity primary key,
	Nombre varchar(50),
	Apellido varchar(50),
	Celular numeric not null,
	Pwd varchar(50) not null,
	Email varchar(100) not null unique,
	Calificacion int default 3,
	Picture varchar,
	create_by varchar(200) default system_user,
	create_on datetime default getdate()
)

create table Usuario_Conductor (
	Id_UsuarioConductor int identity primary key,
	Id_Usuario int foreign key references Usuario(Id_Usuario),
	Fecha_Vencimiento datetime,
	Calificacion varchar(3)
)

create Table Automovil (
	Id_Auto int identity primary key,
	Id_UsuarioConductor int foreign key references Usuario_Conductor(Id_UsuarioConductor),
	Soat bit,
	Placa varchar(8),
	Marca varchar(50),
	Modelo varchar(50),
	Color varchar(50),
	Anho numeric,
	Tarjeta_Propiedad varchar(50),
	create_by varchar(200) default system_user,
	create_on datetime default getdate()
)



create table Viaje (
	Id_Viaje int identity primary key,
	Id_UsuarioConductor int foreign key references Usuario_Conductor(Id_UsuarioConductor),
	Id_Auto int foreign key references Automovil(Id_Auto),
	Fecha_Partida date,
	--Punto_Partida address
	Punto_Partida varchar(50),
	Punto_Llegada varchar(50),
	Is_Active bit default 1,
	create_by varchar(200) default system_user,
	create_on datetime default getdate()
)


create table Viaje_Usuario (
	Id_ViajeUsuario int identity primary key,
	Id_Viaje int foreign key references Viaje(Id_Viaje),
	Id_Usuario int foreign key references Usuario(Id_Usuario),
	Punto_Salida varchar(50),
	Punto_Llegada varchar(50),
	Fecha_Salida datetime,
	Fecha_Llegada datetime default null,
	Forma_Pago varchar(2),
	Is_Active bit default 1,
	create_by varchar(200) default system_user,
	create_on datetime default getdate()
)
