create or replace function Login(
	_Email varchar,
	_Pwd varchar
)
returns table(Id_Usuario int, Nombre varchar, Picture varchar) as $$
begin
	
	return query select Usuario.Id_Usuario, Usuario.Nombre, Usuario.Picture From Usuario where Usuario.Email = _Email and Usuario.Pwd = _Pwd;
end
$$
language plpgsql