create or replace function Ins_Usuario_func(
	_Email varchar(100),
	_Celular numeric,	
	_Pwd varchar(100)
)
returns setof Usuario as $$
begin
	insert into Usuario (Email, Celular, Pwd) values
	(_Email, _Celular , _Pwd);
	return query select 'ok';
end
$$
language plpgsql