USE [UDrive]
GO

/****** Object:  StoredProcedure [dbo].[Ud_Ins_Usuario]    Script Date: 22/03/2022 10:57:21 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Ud_Ins_Usuario]
	-- Add the parameters for the stored procedure here
	@Email varchar(100),
	@Celular numeric,	
	@Pwd varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--declare @var numeric = convert(numeric(20), @Celular)

    -- Insert statements for procedure here
	insert into Usuario (Email, Celular, Pwd) values
	(@Email, @Celular , @Pwd)

	select 'Correct'

END


GO


